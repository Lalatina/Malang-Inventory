var frontApp = new Vue({
    el: '#frontApp',
    data: {
        message: 'Hello Vue.js!',
        scanner: null,
        activeCameraId: null,
        cameras: [],
        scans: 'Belum Ada Data Ke Scan Ya',
      },
      mounted: function () {
        let me = this;
        let opts = {
            continuous: true,
            video: document.getElementById('preview'),
            mirror: false,
            captureImage: true,
            backgroundScan: true,
            refractoryPeriod: 5000,
            scanPeriod: 1
          };          
        me.scanner = new Instascan.Scanner(opts);
        me.scanner.addListener('scan', function (content, image) {
        me.scans = content
        });
        Instascan.Camera.getCameras().then(function (cameras) { 
          me.cameras = cameras;
          if (cameras.length > 0) {
            me.activeCameraId = cameras[0].id;
            me.scanner.start(cameras[0]);
          } else {
            console.error('No cameras found.');
          }
        }).catch(function (e) {
          console.error(e);
          alert('Izinka Camera')
        });
      },
    methods: {
        formatName: function (name) { 
            return name || '(unknown)';
          },        
        bindCamera: function (camera) {            
            this.activeCameraId = camera.id;
            this.scanner.start(camera);
        }
    }
  })