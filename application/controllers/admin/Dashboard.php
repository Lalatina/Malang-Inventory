<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {
        $data['title']="Dashboard";
        $data['subtitle']="";
        if (!$this->session->userdata('logged_in')) {
            redirect('/','refresh');
        }else{
            $this->template->sectionAdmin('pages/admin_page/apps_dashboard',$data);
        }
        
    }

    public function logOut()
    {
        $this->session->sess_destroy();
        redirect('/','refresh');
    }
}
