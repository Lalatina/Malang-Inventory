# Malang-Inventory

Cara Install 
1. Copy / Ganti file .env.example Jadi .env.development
2. Masuk Ke File .env.development Dan Setting Database Mu
3. Jika File .htaccess Tidak Ada Buat File Tersebut Dan Masukkan Code Berikut
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1 [L]
4. Selesai, Ini Udah Auto Deploy Di Sever Dari Branch Master Kalo Ada Update Tolong Buat Branch Baru / Hanya Menggunakan 1 Branch Ajja