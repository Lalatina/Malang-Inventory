<style>
	.errorMassage {
		display: none;
		color: red;
	}
</style>

<section class="content">

	<div class="card">
		<div class="card-header">
			<button data-toggle="modal" data-target="#modal-create" type="button" class="btn btn-success "><i class="fas fa-pencil-alt"></i> Create</button>
			<button onclick="refreshData()" type="button" class="btn btn btn-primary "><i class="fas fa-sync"></i> Refresh</button>
		</div>

		<div class="card-body">
			<table id="project_list" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Username</th>
						<th>Password</th>
						<th>Alamat</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>

	</div>


	<!-- Modal Create -->
	<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<form method="POST" id="createData">
					<div class="modal-header bg-success">
						<h5 class="modal-title">Create <?php echo $title ?></h5>
					</div>
					<div class="modal-body">
						<div class="errorMassage">Formulir Tidak Boleh Ada Yang Kosong</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" name="username" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Alamat</label>
									<textarea name="alamat" class="form-control" rows="5"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success">Create</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Modal Create -->

	<!-- Modal Edit -->
	<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
			<form method="POST" id="updateData">				
				<div class="modal-header bg-primary">
					<h5 class="modal-title">Edit <?php echo $title ?> <span id="id"></span></h5>
				</div>
				<div class="modal-body">
				<div class="errorMassage">Formulir Tidak Boleh Ada Yang Kosong</div>
						<div class="row">
							<input type="hidden" name="id">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" class="form-control" name="nama">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" class="form-control" name="username">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<input type="password" class="form-control" name="password">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Alamat</label>
									<textarea class="form-control" rows="5" name="alamat"></textarea>
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update </button>
				</div>
				</form>				
			</div>
		</div>
	</div>
	<!-- Modal Edit -->

</section>
<script type="text/javascript">
	var table;

	$(document).ready(() => {
		table = $('#project_list').DataTable({
			'aoColumnDefs': [{
				'bSortable': true,
			}],
			"processing": true,
			"serverSide": false,
			"scrollX": true,
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"info": true,
			"autoWidth": false,
			"responsive": true,
			"ajax": {
				"url": "<?php echo base_url('admin/account/listData') ?>",
				"type": "POST",
				"data": function(response) {

				},
			},
			"columnDefs": [{
				"targets": [-1],
				"orderable": true,
			}, ],
		});
	});

	$('#createData').on('submit', (event) => {
		event.preventDefault();
		$.ajax({
			url: "<?php echo base_url('admin/account/createData') ?>",
			type: "POST",
			data: $('#createData').serialize(),
			dataType: "JSON",
			success: ((r) => {

				if (r.message === 'success') {
					$('#modal-create').modal('hide');
					window.alerNotif('Anda Berhasil Tambah Data', window.iconAlertNotif.success)
					document.getElementById("createData").reset()
					hidenAlertValidation()
					reloadTable()
				} else {
					showAlertValidation()
				}

			}),
			error: (error) => {
				window.alerNotif('Ada Sesuatu Yang Error', window.iconAlertNotif.error)
			}
		});
	})

	editData = (val) => {		
				$.ajax({
					url: "<?php echo base_url('admin/account/editData') ?>",
					type: "POST",
					data: {
						'id': val
					},
					dataType: "JSON",
					success: ((r) => {
						if (r.message === 'success') {
							document.forms.updateData['id'].value = r.data.id;
							document.forms.updateData['nama'].value = r.data.nama;
							document.forms.updateData['username'].value = r.data.username;
							document.forms.updateData['password'].value = r.data.password;
							document.forms.updateData['alamat'].value = r.data.alamat;
							$('#modal-edit').modal('show');
						}
					}),
					error: (error) => {
						window.alerNotif('Ada Sesuatu Yang Error', window.iconAlertNotif.error)
					}
				});
	}


	$('#updateData').on('submit', (event) => {
		event.preventDefault();
		$.ajax({
			url: "<?php echo base_url('admin/account/updateData') ?>",
			type: "POST",
			data: $('#updateData').serialize(),
			dataType: "JSON",
			success: ((r) => {

				if (r.message === 'success') {
					$('#modal-edit').modal('hide');
					window.alerNotif('Anda Berhasil Update Data', window.iconAlertNotif.success)
					document.getElementById("updateData").reset()
					hidenAlertValidation()
					reloadTable()
				} else {
					showAlertValidation()
				}

			}),
			error: (error) => {
				window.alerNotif('Ada Sesuatu Yang Error', window.iconAlertNotif.error)
			}
		});
	})

	deleteData = (val) => {
		Swal.fire({
			title: 'Apakah Anda Yakin?',
			text: "Ingin hapus data ini",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#007bff',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Iya, hapus data ini!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: "<?php echo base_url('admin/account/deleteData') ?>",
					type: "POST",
					data: {
						'id': val
					},
					dataType: "JSON",
					success: (async (r) => {
						if (r.message === 'success') {
							$('#modal-edit').modal('hide');
							window.alerNotif('Anda Berhasil Delete Data', window.iconAlertNotif.success)
							reloadTable()
						}
					}),
					error: (error) => {
						window.alerNotif('Ada Sesuatu Yang Error', window.iconAlertNotif.error)
					}
				});

			}
		})
	}


	/* general setiing  */

	reloadTable = () => {
		table.ajax.reload()
	}

	refreshData = () => {
		reloadTable()
	}

	$(".modal").on('hide.bs.modal', () => {
		hidenAlertValidation()
	});

	showAlertValidation = () => {
		let elemnt = document.getElementsByClassName('errorMassage');
    for (var i = 0; i < elemnt.length; i++) {
        elemnt[i].style.display = 'block';
    }		
	}

	hidenAlertValidation = () => {
		let elemnt = document.getElementsByClassName('errorMassage');
    for (var i = 0; i < elemnt.length; i++) {
        elemnt[i].style.display = 'none';
    }		
	}

	/* general setiing  */
</script>