<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sotz</title>
    
    <script src="<?php echo base_url();?>assets/front/js/vue_dev.js"></script>
    <!-- buat product publish -->
    <script src="<?php echo base_url();?>assets/front/js/vue_product.js"></script>

    <script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js">
    </script>
    
</head>

<body>
	<section id="frontApp">
		<section class="cameras">
			<h2>List Kamera</h2>
			<ul>
				<li style="list-style-type: circle;" v-if="cameras.length === 0" class="empty">No cameras found</li>
				<li style="list-style-type: circle;" v-for="camera in cameras">
					<span v-if="camera.id == activeCameraId" :title="formatName(camera.name)"
						style="color:red">{{ formatName(camera.name) }}</span>
					<span v-if="camera.id != activeCameraId" :title="formatName(camera.name)">
						<a @click.stop="bindCamera(camera)">{{ formatName(camera.name) }}</a>
					</span>
				</li>
			</ul>
		</section>
		<section>
			<h2>Hasil Scan</h2>
            {{scans}}
        </section>
        <br>
		<video style="width:100%" id="preview"></video>
	</section>
</body>

<script src="<?php echo base_url();?>assets/front/js/app.js"></script>

</html>