<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Kategori extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title']="Data Kategori";
        $data['subtitle']="Data Kateori";
        if (!$this->session->userdata('logged_in')) {
            redirect('/','refresh');
        }else{
            $this->template->sectionAdmin('pages/admin_page/apps_kategori',$data);
        }
    }
}
