<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CoreBase extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_crud');
    }
    public function index()
    {
        $data['title']="Dashboard";
        if (!$this->session->userdata('logged_in')) {
            $this->load->view('apps_login');
        }else{
            redirect('admin/dashboard/');
        }
    }

    public function login()
    {
        $username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		if($username != "" || $password != "") {
            $data=$this->Model_crud->view_query("SELECT * FROM tb_users  WHERE username='$username' AND password='$password'")->row();
            if ($data) {
                $data=array(
                    'id'=>$data->id,
                    'username'=>$data->username,
                    'password'=>$data->password,
                    'logged_in'=>TRUE);
                $this->session->set_userdata($data);            
                redirect('admin/dashboard/');
            }else{
                redirect('/');
            }

		}else{
            redirect('/');
        }

    }
}
