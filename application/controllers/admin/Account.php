<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Account extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title'] = 'Data Akun Admin';
        $data['subtitle'] = 'Data Akun Admin';
        if (!$this->session->userdata('logged_in')) {
            redirect('/', 'refresh');
        } else {
            $this->template->sectionAdmin('pages/admin_page/apps_account', $data);
        }
    }

    public function listData()
    {
        $list = $this->Model_crud->view_query("SELECT * FROM tb_users")->result();
        $data = array();
        $no = 1;
        $draw = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

        $option = array();
        $noo = 1;
        foreach ($list as $rowData) {
            $data[] = [
                $noo,
                $rowData->nama,
                $rowData->username,
                $rowData->password = '******************',
                $rowData->alamat,
                '<div class="text-center">
                <button  onclick="editData(' . $rowData->id . ')" type="button" class="btn btn-primary">
                <li class="fas fa-edit"></li>
                </button>
                 
                <button onclick="deleteData(' . $rowData->id . ')" type="button" class="btn btn-danger ">
                <li class="fas fa-trash"></li>
                </button>
                </div>',
            ];
            $noo++;
        }

        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        return response([
            'status' => 200,
            'message' => 'success',
            'draw' => $draw,
            'recordsTotal' => count($list),
            'recordsFiltered' => count($list),
            'data' => $data
        ], 200);
    }

    public function createData()
    {
        $postData = new StdClass();
        $postData->nama = $this->input->post('nama');
        $postData->username = $this->input->post('username');
        $postData->password = md5($this->input->post('password'));
        $postData->alamat = $this->input->post('alamat');
        $configFormValidation = array(
            array(
                'field' => 'nama',
                'label' => 'nama',
                'rules' => 'required',
            ),
            array(
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required',
            ),
            array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required',
            ),
            array(
                'field' => 'alamat',
                'label' => 'alamat',
                'rules' => 'required',
            ),
        );
        $this->form_validation->set_rules($configFormValidation);
        if($this->form_validation->run()){
        $outPut = $this->Model_crud->insert($postData, "tb_users");
        if ($outPut) {
            return response([
                'status' => 200,
                'message' => 'success',
                'data' => ''
            ], 200);
        } else {
            return response([
                'status' => 400,
                'message' => 'error',
                'data' => ''
            ], 400);
        }
        }else{
               return response([
                'status' => 200,
                'message' => 'error',
                'data' => ''
            ], 200);
        }
    }

    public function editData()
    {
        $id = $this->input->post('id');
        $outPut = $this->Model_crud->view_query("SELECT * FROM tb_users WHERE id = '$id' ")->row();
        if ($outPut) {
            return response([
                'status' => 200,
                'message' => 'success',
                'data' => $outPut
            ], 200);
        } else {
            return response([
                'status' => 400,
                'message' => 'error',
                'data' => ''
            ], 400);
        }
    }

    public function updateData()
    {
        $where['id'] = $this->input->post('id');
        $postData = new StdClass();
        $postData->nama = $this->input->post('nama');
        $postData->username = $this->input->post('username');
        $postData->password = md5($this->input->post('password'));
        $postData->alamat = $this->input->post('alamat');
        $configFormValidation = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required',
            ),            
            array(
                'field' => 'nama',
                'label' => 'nama',
                'rules' => 'required',
            ),
            array(
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required',
            ),
            array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required',
            ),
            array(
                'field' => 'alamat',
                'label' => 'alamat',
                'rules' => 'required',
            ),
        );
        $this->form_validation->set_rules($configFormValidation);

        if($this->form_validation->run()){
            $outPut = $this->Model_crud->update($postData, $where, 'tb_users');            
            if ($outPut) {
                return response([
                    'status' => 200,
                    'message' => 'success',
                    'data' => ''
                ], 200);
            } else {
                return response([
                    'status' => 400,
                    'message' => 'error',
                    'data' => ''
                ], 400);
            }
            }else{
                   return response([
                    'status' => 200,
                    'message' => 'error',
                    'data' => ''
                ], 200);
            }
    }

    public function deleteData()
    {
        $where['id'] = $this->input->post("id");
        $outPut = $this->Model_crud->delete($where, 'tb_users');
        if ($outPut) {
            return response([
                'status' => 200,
                'message' => 'success',
                'data' => ''
            ], 200);
        } else {
            return response([
                'status' => 400,
                'message' => 'error',
                'data' => ''
            ], 400);
        }
    }
}
