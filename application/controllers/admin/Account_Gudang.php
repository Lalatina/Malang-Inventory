<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Account_Gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title']="Data Akun Gudang";
        $data['subtitle']="Data Akun Gudang";
        $data['resultData']=$this->Model_crud->view_query("SELECT * FROM tb_users")->result();
        if (!$this->session->userdata('logged_in')) {
            redirect('/','refresh');
        }else{
            $this->template->sectionAdmin('pages/admin_page/apps_account_gudang',$data);
        }      
        
    }

    public function listData()
    {
        $list = $this->Model_crud->view_query("SELECT * FROM tb_users")->result();        
        $data = array();
        $i = 1;
        $no = 1;
        $draw = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

        $option = array();
        $noo = 1;
        foreach ($list as $row_data) {
            $data[] = [
                $noo,
                $row_data->nama,
                $row_data->username,
                $row_data->password = '******************',
                $row_data->alamat,
                '<div class="text-center">
                <button  onclick="editData('.$row_data->id.')" type="button" class="btn btn-primary">
                <li class="fas fa-edit"></li>
                </button>
                 
                <button onclick="deleteData('.$row_data->id.')" type="button" class="btn btn-danger ">
                <li class="fas fa-trash"></li>
                </button>
                </div>',
                
            ];
            $noo++;
        }

        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $outPut = array(
                    "draw" => $draw,
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data
                );
        echo json_encode($outPut);  
    }

    public function createData()
    {
        $postData = new StdClass();
        $postData->nama = $this->input->post("nama");
        $postData->username = $this->input->post("username");
        $postData->password = md5($this->input->post("password"));
        $postData->alamat = $this->input->post("alamat");
        $outPut = $this->Model_crud->insert($postData, "tb_users");
        if ($outPut) {
            $outPutResponse =  array(
                 "data" => 'success',
             );
             echo json_encode($outPutResponse);
         }else{
             $outPutResponse =  array(
                 "data" => 'error',
             );
             echo json_encode($outPutResponse);
         }
    }

    public function editData()
    {
        $id = $this->input->post("id");
        $outPut = $this->Model_crud->view_query("SELECT * FROM tb_users WHERE id = '$id' ")->row();
        echo json_encode($outPut); 
    }

    public function updateData()
    {
        $where['id'] = $this->input->post("id");
        $postData = new StdClass();
        $postData->nama = $this->input->post("nama");
        $postData->username = $this->input->post("username");
        $postData->password = md5($this->input->post("password"));
        $postData->alamat = $this->input->post("alamat");
        $outPut=$this->Model_crud->update($postData,$where,'tb_users');
        if ($outPut) {
           $outPutResponse =  array(
                "data" => 'success',
            );
            echo json_encode($outPutResponse);
        }else{
            $outPutResponse =  array(
                "data" => 'error',
            );
            echo json_encode($outPutResponse);
        }
    }
    
    public function deleteData()
    {
        $where['id'] = $this->input->post("id");
        $outPut=$this->Model_crud->delete($where,'tb_users');
        if ($outPut) {
            $outPutResponse =  array(
                 "data" => 'success',
             );
             echo json_encode($outPutResponse);
         }else{
             $outPutResponse =  array(
                 "data" => 'error',
             );
             echo json_encode($outPutResponse);
         }
    }
}
