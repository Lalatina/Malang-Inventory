<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_crud extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function view($table, $order, $type)
	{
		$this->db->order_by($order, $type);
		$data = $this->db->get($table);
		return $data;
	}
	function view_query($query)
	{
		$data = $this->db->query($query);
		return $data;
	}

	function view_data_where($table, $where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		return $data;
	}

	function view_data_where_order($table, $where, $order, $type)
	{
		$this->db->where($where);
		$this->db->order_by($order, $type);
		$data = $this->db->get($table);
		return $data;
	}

	function view_data_join($select, $from, $join, $order, $type)
	{

		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join);
		$this->db->order_by($order, $type);
		$data = $this->db->get();
		return $data;
	}

	function getGambar($where, $tabel)
	{
		$data = $this->db->where($where);
		$data = $this->db->get($tabel);
		return $data;
	}

	function insert($data, $tabel)
	{
		$add = $this->db->insert($tabel, $data);
		if ($add) {
			return true;
		} else {
			return false;
		}
	}

	function update($data, $where, $tabel)
	{
		$this->db->where($where);
		$update = $this->db->update($tabel, $data);
		if ($update) {
			return true;
		} else {
			return false;
		}
	}

	function delete($where, $tabel)
	{
		$this->db->where($where);
		$delete = $this->db->delete($tabel);
		if ($delete) {
			return true;
		} else {
			return false;
		}
	}
}
