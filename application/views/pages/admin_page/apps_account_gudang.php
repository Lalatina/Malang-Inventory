<section class="content">
	<div class="card">
		<div class="card-header">
			<button data-toggle="modal" data-target="#modal-create" type="button" class="btn btn-success "><i
					class="fas fa-pencil-alt"></i> Create</button>
		</div>
		<div class="card-body">
			<table id="project_list" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Username</th>
						<th>Password</th>
						<th>Alamat</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>


	<!-- Modal Create -->
	<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<h5 class="modal-title">Create <?php echo $title ?></h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" id="nama" class="form-control" placeholder="Nama">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Username</label>
								<input type="text" id="username" class="form-control" placeholder="Username">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" id="password" class="form-control" placeholder="Password">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Alamat</label>
								<textarea id="alamat" class="form-control" rows="5"></textarea>
							</div>
						</div>			
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" onclick="createData()" class="btn btn-success">Create</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Create -->

	<!-- Modal Edit -->
	<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<h5 class="modal-title">Edit <?php echo $title ?> <span id="idEdit"></span></h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" placeholder="Nama" id="namaEdit">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Username</label>
								<input type="text" class="form-control" placeholder="Username" id="usernameEdit">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Password" id="passwordEdit">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" rows="5" id="alamatEdit"></textarea>
							</div>
						</div>			
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" onclick="updateData()" class="btn btn-primary">Update </button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Edit -->

</section>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
     
      table = $('#project_list').DataTable({
        'aoColumnDefs': [{
          'bSortable': false,
        }],
        "processing": true,
        "serverSide": false,
		"scrollX": true,
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,		
        "ajax": {
          "url": "<?php echo base_url('admin/account/listData') ?>",
          "type": "POST",
          "data": function(response) {
			response.user = 0
          },
        },
        "columnDefs": [
          {
            "targets": [ -1 ],
            "orderable": false,
          },
        ],
      });

	});

	createData = () => {
		let postData = {
			'nama': document.getElementById("nama").value,
			'username': document.getElementById("username").value,
			'password': document.getElementById("password").value,
			'alamat': document.getElementById("alamat").value,
		}

		$.ajax({
        url: "<?php echo base_url('admin/account/createData') ?>",
		type: "post",
		data: postData,
		dataType : "json",
        success:  (async (r)=> {
			if (r.data === "success") {
				await $('#modal-create').modal('hide');
				await Swal.fire({
				icon: 'success',
				title: 'Anda Berhasil Tambah Data',
				showConfirmButton: false,
				})
				 document.getElementById("nama").value = ''
				 document.getElementById("username").value = ''
				 document.getElementById("password").value = ''
				 document.getElementById("alamat").value = ''
				reloadTable()
			}
			
		}), 
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
		
	}
	editData = (val) => {
		
		$.ajax({
        url: "<?php echo base_url('admin/account/editData') ?>",
		type: "post",
		data: {'id':val},
		dataType : "json",
        success:  (async (r)=> {
			await $('#modal-edit').modal('show');
			document.getElementById("idEdit").value = r.id ;
			document.getElementById("namaEdit").value = r.nama ;
			document.getElementById("usernameEdit").value = r.username ;
			document.getElementById("passwordEdit").value = r.password ;
			document.getElementById("alamatEdit").value = r.alamat ;
		}), 
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
		
	}

	updateData = () => {
		let postData = {
			'id': document.getElementById("idEdit").value,
			'nama': document.getElementById("namaEdit").value,
			'username': document.getElementById("usernameEdit").value,
			'password': document.getElementById("passwordEdit").value,
			'alamat': document.getElementById("alamatEdit").value,
		}
		$.ajax({
        url: "<?php echo base_url('admin/account/updateData') ?>",
		type: "post",
		data: postData,
		dataType : "json",
        success:  (async (r)=> {
			if (r.data === "success") {
				await $('#modal-edit').modal('hide');

				await Swal.fire({
				icon: 'success',
				title: 'Anda Berhasil Update Data',
				showConfirmButton: false,
				})

				reloadTable()
			}
			
		}), 
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	}

	deleteData = (val) => {
		Swal.fire({
			title: 'Apakah Anda Yakin?',
			text: "Ingin hapus data ini",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#007bff',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Iya, hapus data ini!'
		}).then((result) => {
			if (result.value) {


				$.ajax({
					url: "<?php echo base_url('admin/account/deleteData') ?>",
					type: "post",
					data: {'id':val},
					dataType: "json",
					success: (async (r) => {
						if (r.data === "success") {

							await $('#modal-edit').modal('hide');

							await Swal.fire({
								icon: 'success',
								title: 'Anda Berhasil Delete Data',
								showConfirmButton: false,
							})

							reloadTable()
						}

					}),
					error: function (jqXHR, textStatus, errorThrown) {
						console.log(textStatus, errorThrown);
					}
				});

			}
		})
	}

	 reloadTable = () => {
      table.ajax.reload()
	}
	
</script>