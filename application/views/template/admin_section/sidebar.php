<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="<?php echo base_url();?>admin/dashboard" class="brand-link">
		<img src="<?php echo base_url();?>assets/img/logo.png" alt="AdminLTE Logo"
			class="brand-image img-circle elevation-3">
		<span class="brand-text font-weight-light">Management Stock </span>
	</a>
	<div class="sidebar">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="<?php echo base_url();?>assets/admin-lte/dist/img/user2-160x160.jpg"
					class="img-circle elevation-2" alt="User Image">
			</div>
			<div class="info">
				<a href="<?php echo base_url();?>admin/dashboard" class="d-block">Admin</a>
			</div>
		</div>
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

				<li class="nav-item has-treeview">
					<a href="<?php echo base_url();?>admin/dashboard" class="nav-link <?=($this->uri->segment(2)==='dashboard')?'active':''?>">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Dasboard
						</p>
					</a>
				</li>
					<!-- data master -->
				<li class="nav-item has-treeview <?=($this->uri->segment(2)==='Gudang' || $this->uri->segment(2)==='Satuan' || $this->uri->segment(2)==='Kategori' ) ?'menu-open':''?>">

					<a href="javascript:void(0)" class="nav-link <?=($this->uri->segment(2)==='Gudang' || $this->uri->segment(2)==='Satuan' || $this->uri->segment(2)==='Kategori')?'active':''?>">
					
						<i class="nav-icon fas fa-archive"></i>
						<p>
							Data Master
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/Gudang" class="nav-link <?=($this->uri->segment(2)==='Gudang')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Gudang</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/Satuan" class="nav-link <?=($this->uri->segment(2)==='Satuan')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Satuan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/Kategori" class="nav-link <?=($this->uri->segment(2)==='Kategori')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Kategori</p>
							</a>
						</li>
					</ul>
				</li>	
						<!-- master akun -->
					<li class="nav-item has-treeview <?=($this->uri->segment(2)==='karyawan' || $this->uri->segment(2)==='Account' || $this->uri->segment(2)==='Account_Spv' || $this->uri->segment(2)==='Account_Gudang' ) ?'menu-open':''?>">

					<a href="javascript:void(0)" class="nav-link <?=($this->uri->segment(2)==='karyawan' || $this->uri->segment(2)==='Account' || $this->uri->segment(2)==='Account_Spv' || $this->uri->segment(2)==='Account_Gudang' )?'active':''?>">

					<i class="nav-icon fas fa-users"></i>
						<p>
							Data Akun
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>					
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/karyawan" class="nav-link <?=($this->uri->segment(2)==='karyawan')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Karyawan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/Account" class="nav-link <?=($this->uri->segment(2)==='Account')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Akun Admin</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/Account_Spv" class="nav-link <?=($this->uri->segment(2)==='Account_Spv')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Akun Supervisor</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url();?>admin/Account_Gudang" class="nav-link <?=($this->uri->segment(2)==='Account_Gudang')?'active':''?>">
								<i class="far fa-circle nav-icon"></i>
								<p>Data Akun Gudang</p>
							</a>
						</li>											
					</ul>
				</li>	
						<!-- master barang -->
				<li class="nav-item has-treeview <?=($this->uri->segment(2)==='supplier')?'menu-open':''?>">
					<a href="javascript:void(0)" class="nav-link <?=($this->uri->segment(2)==='supplier')?'active':''?>">
						<i class="nav-icon fas fa-shopping-cart"></i>
						<p>
							Data Barang
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Barang Riset</p>
							</a>
						</li>					
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Barang Jual</p>
							</a>
						</li>					
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Barang Exp</p>
							</a>
						</li>					
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Obname Barang</p>
							</a>
						</li>					
					</ul>
				</li>
				 <!-- master laporan -->
				<li class="nav-item has-treeview ">
					<a href="javascript:void(0)" class="nav-link">
						<i class="nav-icon fas fa-cash-register"></i>
						<p>
							Data Laporan
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Sisa Barang</p>
							</a>
						</li>											
					</ul>
				</li>

			</ul>
		</nav>
	</div>
</aside>

<div class="content-wrapper">
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
            <h1><?php echo $title; ?></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard" >Home</a></li>
					<li class="breadcrumb-item active"><?php echo $subtitle ?></li>
				</ol>
			</div>
		</div>
	</div>
</section>