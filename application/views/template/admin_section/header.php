
	<div class="wrapper">

		<nav class="main-header navbar navbar-expand navbar-dark navbar-primary">
			<ul class="navbar-nav">
				<!-- <li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
				</li> -->
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown show">
          <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
          <!-- <i class="fas fa-sign-out-alt"></i> -->
          <i class="fas fa-cogs"></i>
        </a>  
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
          <span class="dropdown-item dropdown-header text-left">Setting</span>
          <div class="dropdown-divider "></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> Akun
            
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url('admin/dashboard/logOut');?>" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i> Log Out
           
          </a>
                       
        </li>
         
			</ul>
		</nav>
