<section class="content">
	<div class="card">
		<div class="card-header">
			<button data-toggle="modal" data-target="#modal-create" type="button" class="btn btn-success "><i
					class="fas fa-pencil-alt"></i> Create</button>
		</div>
		<div class="card-body">
			<table id="data-tables" class="table table-bordered table-hover">
				<thead>
					<tr>
					    <th>Id kategori</th>
						<th>Kategori</th>
						<th>Status</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
				<tr>
						<td>0001</td>
						<td>Jagung</td>
						<td>Aktif</td>
						<td class="text-center">
							<button data-toggle="modal" data-target="#modal-edit" type="button" class="btn btn-primary">
								<li class="fas fa-edit"></li>
							</button>
							<button onclick="deleteData()" type="button" class="btn btn-danger ">
								<li class="fas fa-trash"></li>
							</button></td>
					</tr>
					<tr>
						<td>0002</td>
						<td>Padi</td>
						<td>Aktif</td>
						<td class="text-center">
							<button data-toggle="modal" data-target="#modal-edit" type="button" class="btn btn-primary">
								<li class="fas fa-edit"></li>
							</button>
							<button onclick="deleteData()" type="button" class="btn btn-danger ">
								<li class="fas fa-trash"></li>
							</button></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>


	<!-- Modal Create -->
	<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<h5 class="modal-title">Create <?php echo $title ?></h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama Supplier</label>
								<input type="text" class="form-control" placeholder="Nama Supplier">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>No Telepon</label>
								<input type="number" class="form-control" placeholder="No Telepon">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control" placeholder="Email">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" rows="5"></textarea>
							</div>
						</div>			
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success">Create</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Create -->

	<!-- Modal Edit -->
	<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<h5 class="modal-title">Edit <?php echo $title ?></h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama Supplier</label>
								<input type="text" class="form-control" placeholder="Nama Supplier">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>No Telepon</label>
								<input type="number" class="form-control" placeholder="No Telepon">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control" placeholder="Email">
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" rows="5"></textarea>
							</div>
						</div>			
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Update </button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Edit -->

</section>
<script type="text/javascript">
	deleteData = () => {
		window.AlertDelete()
	}
</script>