    var iconAlertNotif = {
    	success: 'success',
    	error: 'error',
    	warning: 'warning',
    	question: 'question',
    	info: 'info',
    }

    $(document).ready(() => {
    	$(".modal").modal({
    		show: false,
    		backdrop: 'static'
    	});
    });


    alerNotif = (body, icon) => {
    	Swal.fire({
    		title: 'Notifications',
    		text: body,
    		icon: icon,
    	})
    }
