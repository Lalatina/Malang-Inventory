<?php

class Template
{
    protected $_ci;


    public function __construct()
    {
        $this->_ci=&get_instance();
    }

 // admin //
    public function sectionAdmin($template, $data=null)
    {
        $data['content']=$this->_ci->load->view($template, $data, true);
        $data['header']=$this->_ci->load->view('template/admin_section/header', $data, true);
        $data['sidebar']=$this->_ci->load->view('template/admin_section/sidebar', $data, true);
        $data['footer']=$this->_ci->load->view('template/admin_section/footer', $data, true);
        $this->_ci->load->view('/core_admin', $data);
    }
}
