<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Stock_Gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title']="Data Stock Gudang";
        $data['subtitle']="Data Stock Gudang";
        if (!$this->session->userdata('logged_in')) {
            redirect('/','refresh');
        }else{
            $this->template->sectionAdmin('pages/admin_page/apps_stock_gudang',$data);
        }

    }
}
